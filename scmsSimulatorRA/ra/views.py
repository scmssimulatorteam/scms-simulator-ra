from django.shortcuts import render
from scmsSimulatorRA.settings.base import CURRENT_SETTING, SECOND_MODE, THIRD_MODE
from .models import VehicleTransaction
from .models import RaButterflyTransaction


def index(request):
    if(CURRENT_SETTING == SECOND_MODE):
        transactions = VehicleTransaction.objects.all().order_by('-transaction_date', 'request__vehicle_id')
        context = {
            'transactions': transactions
        }
        return render(request, 'ra/index_mode_2.html', context)
    elif(CURRENT_SETTING == THIRD_MODE):
        transactions = RaButterflyTransaction.objects.all().order_by('-key_expansions__pca_transaction__transaction_date', 'key_expansions__vehicle_request__vehicle_id')
        context = {
            'transactions': transactions
        }
        return render(request, 'ra/index_mode_3.html', context)
    else:
        context = {}
        return render(request, 'error.html', context)
