import json
from websocket import create_connection
from scmsSimulatorRA.settings import second, third
from scmsSimulatorRA.settings.base import CURRENT_SETTING, SECOND_MODE, THIRD_MODE


def send_message(message):
    if(CURRENT_SETTING == SECOND_MODE):
        host = second.SOCKET_HOST_NAME
        port = str(second.PORT_PCA)
    elif(CURRENT_SETTING == THIRD_MODE):
        host = third.SOCKET_HOST_NAME
        port = str(third.PORT_PCA)
    else:
        return None

    # print("PORTA: %s" % port)
    ws = create_connection("ws://" + host + ":" + port + "/ws/pca/")
    # print("Sending message: %s" % message)
    message_string = json.dumps(message)

    ws.send(message_string)
    # print("Sent")

    received = ws.recv()
    # print("Received: %s\n" % received)

    ws.close()

    json_msg = json.loads(received)
    if('message' in json_msg):
        return json_msg.get('message')
    else:
        return None


def send_message_LA(message, la_number):
    host = third.SOCKET_HOST_NAME
    if(la_number == 1):
        port = str(third.PORT_LA_1)
    elif(la_number == 2):
        port = str(third.PORT_LA_2)
    else:
        return None
    # print("PORTA: %s" % port)
    ws = create_connection("ws://" + host + ":" + port + "/ws/la/")
    # print("Sending message: %s" % message)
    message_string = json.dumps(message)

    ws.send(message_string)
    # print("Sent")

    received = ws.recv()
    # print("Received: %s\n" % received)

    ws.close()

    json_msg = json.loads(received)
    if('message' in json_msg):
        return json_msg.get('message')
    else:
        return None
