from .models import PublicKey
from .models import PrivateKey
from .models import KeyPair
from .models import OwnerPublicKey
from .models import VehicleRequest
from .models import VehicleReturnValue
from .models import VehicleTransaction
from .models import VehicleButterflyRequest
from .models import LaButterflyTransaction
from .models import PcaButterflyTransaction
from .models import KeyExpansion
from .models import RaButterflyTransaction
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import Point


def clean_string(list_of_elements):
    return str(list_of_elements).replace(" ", "")


def save_own_keys(private_key: BigNumber, public_key: Point):
    pk_bin_value = clean_string(private_key.write_bin())
    cert_pk = PrivateKey.objects.create(bin_value=pk_bin_value)

    pu_bin_value = clean_string(public_key.write_bin())
    cert_pu = PublicKey.objects.create(bin_value=pu_bin_value)

    OwnerPublicKey.objects.create(belong_to='R', public_key=cert_pu)
    keypair = KeyPair.objects.create(private_key=cert_pk, public_key=cert_pu)

    return keypair


def get_PCA_public_key():
    owner_pca = OwnerPublicKey.objects.filter(belong_to='P')
    if not owner_pca:
        return None

    chave_publ_PCA = Point()

    public_list = owner_pca[0].public_key.bin_value.strip('[]').split(",")
    public_bin = [int(i) for i in public_list]

    chave_publ_PCA.read_bin(public_bin)
    return chave_publ_PCA


def get_LA_1_public_key():
    owner_la = OwnerPublicKey.objects.filter(belong_to='L')
    if not owner_la:
        return None

    chave_publ_LA = Point()

    public_list = owner_la[0].public_key.bin_value.strip('[]').split(",")
    public_bin = [int(i) for i in public_list]

    chave_publ_LA.read_bin(public_bin)
    return chave_publ_LA


def get_LA_2_public_key():
    owner_la = OwnerPublicKey.objects.filter(belong_to='M')
    if not owner_la:
        return None

    chave_publ_LA = Point()

    public_list = owner_la[0].public_key.bin_value.strip('[]').split(",")
    public_bin = [int(i) for i in public_list]

    chave_publ_LA.read_bin(public_bin)
    return chave_publ_LA


def get_RA_keypair_values():
    has_key = OwnerPublicKey.objects.filter(belong_to='R')
    if(not has_key):
        return None, None
    else:
        keypair = KeyPair.objects.filter(public_key=has_key[0].public_key)[0]

        public_list = keypair.public_key.bin_value.strip('[]').split(",")
        public_bin = [int(i) for i in public_list]

        private_list = keypair.private_key.bin_value.strip('[]').split(",")
        private_bin = [int(i) for i in private_list]

        return private_bin, public_bin


def save_vehicle_transaction(vehicle_id, pseud_point, cypher_point,
                             request_id, reconstruction_point, cyphertext):
    vehicleRequest = save_vehicle_request(vehicle_id, pseud_point, cypher_point)
    vehicleReturnValue = save_vehicle_return_value(reconstruction_point, cyphertext, request_id)

    vehicleTransaction = VehicleTransaction.objects.create(request=vehicleRequest,
                                                           return_value=vehicleReturnValue)
    return vehicleTransaction


def save_vehicle_request(vehicle_id, pseud_point, cypher_point):
    pseud_point_bin_value = clean_string(pseud_point)
    cypher_bin_value = clean_string(cypher_point)
    vehicle_id_bin_value = clean_string(vehicle_id)

    vehicleRequest = VehicleRequest.objects.create(vehicle_id=vehicle_id_bin_value,
                                                   pseud_point=pseud_point_bin_value,
                                                   cypher_point=cypher_bin_value)
    return vehicleRequest


def save_vehicle_return_value(reconstruction_point, cyphertext, request_id):
    reconstruction_point_bin_value = clean_string(reconstruction_point)
    cyphertext_value = clean_string(cyphertext)
    request_id_value = clean_string(request_id)

    vehicleReturnValue = VehicleReturnValue.objects.create(request_id=request_id_value,
                                                           reconstruction_point=reconstruction_point_bin_value,
                                                           cyphertext=cyphertext_value)
    return vehicleReturnValue


def save_vehicle_butterfly_request(vehicle_id, pseud_point, cypher_point, key_p, key_c):
    pseud_point_bin_value = clean_string(pseud_point)
    cypher_bin_value = clean_string(cypher_point)
    key_p_bin_value = clean_string(key_p)
    key_c_bin_value = clean_string(key_c)
    vehicle_id_bin_value = clean_string(vehicle_id)

    vehicleRequest = VehicleButterflyRequest.objects.create(vehicle_id=vehicle_id_bin_value,
           pseud_point=pseud_point_bin_value,
           cypher_point=cypher_bin_value,
           key_p=key_p_bin_value,
           key_c=key_c_bin_value)
    return vehicleRequest


def save_la_butterfly_transaction(vehicle_request, la_number, request_id, period_i, j_certificates, cyphertext, reconstruction_point):
    request_id_bin_value = clean_string(request_id)
    reconstruction_point_bin_value = clean_string(reconstruction_point)
    cyphertext_value = clean_string(cyphertext)

    la_transaction = LaButterflyTransaction.objects.create(
        vehicle_request=vehicle_request,
        la_number=la_number,
        request_id=request_id_bin_value,
        period_i=period_i,
        j_certificates=j_certificates,
        cyphertext=cyphertext_value,
        reconstruction_point=reconstruction_point_bin_value)
    return la_transaction


def save_key_expansion(vehicle_request, pca_request_id, expansion_number, iv_p_1, iv_p_2, iv_c_1, iv_c_2, la_cyphertext_1, la_recovery_point_1, la_cyphertext_2, la_recovery_point_2):
    la_c1 = clean_string(la_cyphertext_1)
    la_c2 = clean_string(la_cyphertext_2)
    la_rp1 = clean_string(la_recovery_point_1)
    la_rp2 = clean_string(la_recovery_point_2)

    # print("IV TIPO: %s" % type(iv_p_1))
    iv_p1 = clean_string(list(iv_p_1))
    iv_p2 = clean_string(list(iv_p_2))
    iv_c1 = clean_string(list(iv_c_1))
    iv_c2 = clean_string(list(iv_c_2))

    empty_pca_request = PcaButterflyTransaction.objects.create(pca_request_id=pca_request_id)

    key_expansion = KeyExpansion.objects.create(
        vehicle_request=vehicle_request,
        pca_transaction=empty_pca_request,
        expansion_number=expansion_number,
        iv_p_1=iv_p1,
        iv_p_2=iv_p2,
        iv_c_1=iv_c1,
        iv_c_2=iv_c2,
        la_cyphertext_1=la_c1,
        la_recovery_point_1=la_rp1,
        la_cyphertext_2=la_c2,
        la_recovery_point_2=la_rp2)
    return key_expansion


def update_pca_request(pca_request_id, cyphertext, recovery_point):
    pca_transaction = PcaButterflyTransaction.objects.filter(pca_request_id=pca_request_id)
    if(not pca_transaction):
        return None
    cyphertext_clean = clean_string(cyphertext)
    recovery_point_clean = clean_string(recovery_point)
    pca_transaction.update(cyphertext=cyphertext_clean,
        recovery_point=recovery_point_clean)
    return pca_transaction[0]


def get_key_expansion_by_pca_request(pca_request):
    key_expansion = KeyExpansion.objects.filter(pca_transaction__pca_request_id=pca_request)
    if(key_expansion):
        key = key_expansion[0]
        # print("Key IVp1: %s" % type(key.iv_p_1))

        iv_p_1 = key.iv_p_1.strip('[]').split(",")
        iv_p_1_bin = [int(i) for i in iv_p_1]
        key.iv_p_1 = iv_p_1_bin

        # print("Key IVp1: %s" % type(key.iv_p_1))

        iv_p_2 = key.iv_p_2.strip('[]').split(",")
        iv_p_2_bin = [int(i) for i in iv_p_2]
        key.iv_p_2 = iv_p_2_bin

        iv_c_1 = key.iv_c_1.strip('[]').split(",")
        iv_c_1_bin = [int(i) for i in iv_c_1]
        key.iv_c_1 = iv_c_1_bin

        iv_c_2 = key.iv_c_2.strip('[]').split(",")
        iv_c_2_bin = [int(i) for i in iv_c_2]
        key.iv_c_2 = iv_c_2_bin

        return key
    return None


def create_ra_butterlfy_transaction(la_1_transaction, la_2_transaction):
    return RaButterflyTransaction.objects.create(la_1_transaction=la_1_transaction,
        la_2_transaction=la_2_transaction)
