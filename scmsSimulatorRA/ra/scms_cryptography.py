from django.db import transaction
from pyrelic.relic_wrapper import BigNumber
from pyrelic.relic_wrapper import Point
from pyrelic.relic_wrapper import CryptoProtocols
from pyrelic.relic_wrapper import Hash
from pyrelic.relic_constants import BN_SIZE_ARRAY
from scmsSimulatorRA.settings.base import N_CERTIFICATES_SIMULTANEOUSLY
from . import websocket
from . import bd_conversion
import os


@transaction.atomic
def handle_vehicle_solicitation_second(vehicle_request):
    """
        SEGUNDO MODO DE OPERAÇÃO

        Método que gerencia as solicitações dos veículos
        e solicita os pseudônimos para a PCA.
        Sem butterfly key expansion.

        Recebe mensagem com:
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.

        Após a decifração de cyphertext, obtemos a estrutura:
            - 'vehicle_id': Chave pública vitálicia (ponto) do veículo
            - 'pseud_point': Ponto que deve ser assinado,
            - 'cypher_point': Ponto que deve cifrar o pacote de retorno

        A RA cria um identificador para o veículo a partir do 'vehicle_id'
        e utilizará esse identificador na mensagem para PCA.

        Monta um pacote com:
        {
            request_id,
            pseud_point,
            cypher_point
        }
        e cifra com a chave pública da PCA. O pacote enviado à PCA
        contém o recovery_point da cifração e seu respectivo cyphertext.

        Recebe da PCA o pacote com:
            - 'request_id': identificador da requisição,
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.
        A RA não consegue decifrar cyphertext por não possuir a chave privada
        de cypher_point.

        RA encaminha o pacote sem o request_id para o veículo.
    """
    private, public = get_own_keys()

    bin_value = vehicle_request.get('recovery_point')
    recovery_point = Point()
    recovery_point.read_bin(bin_value)

    cyphertext = vehicle_request.get('cyphertext')

    decrypted_message = CryptoProtocols.ecies_decrypt(recovery_point, cyphertext, private)

    identification = decrypted_message.get('vehicle_id')
    pseudonym_bin = decrypted_message.get('pseud_point')

    cypherkey = decrypted_message.get('cypher_point')

    hash_text = identification + pseudonym_bin
    request_id = Hash.hash_sha256(hash_text)

    plain_message = {
        'request_id': request_id,
        'pseud_point': pseudonym_bin,
        'cypher_point': cypherkey
    }

    chave_publ_PCA = bd_conversion.get_PCA_public_key()
    # print("Chave pública PCA: %s" % chave_publ_PCA)
    encrypted_message, ponto_resultante = CryptoProtocols.ecies_encrypt(chave_publ_PCA, plain_message)

    message_to_send = {
        'recovery_point': ponto_resultante.write_bin(),
        'cyphertext': encrypted_message
    }

    # TODO? SHUFFLE DE MENSAGENS DE DIFERENTES VEÍCULOS ANTES DE ENVIAR
    received_message_PCA = websocket.send_message(message_to_send)

    bin_value_PCA = received_message_PCA.get('recovery_point')
    recovery_point_PCA = Point()
    recovery_point_PCA.read_bin(bin_value_PCA)

    cyphertext_PCA = received_message_PCA.get('cyphertext')

    decrypted_message_PCA = CryptoProtocols.ecies_decrypt(recovery_point_PCA, cyphertext_PCA, private)

    request_id_PCA = decrypted_message_PCA.get('request_id')
    recovery_point_to_vehicle = decrypted_message_PCA.get('recovery_point')
    cyphertext_to_vehicle = decrypted_message_PCA.get('cyphertext')

    if(request_id_PCA == request_id):
        # SALVAR NO BD
        bd_conversion.save_vehicle_transaction(identification, pseudonym_bin, cypherkey,
                                 request_id, recovery_point_to_vehicle, cyphertext_to_vehicle)

        message_to_vehicle = {
            'recovery_point': recovery_point_to_vehicle,
            'cyphertext': cyphertext_to_vehicle
        }

        return message_to_vehicle
    return {}


@transaction.atomic
def handle_vehicle_solicitation_third(vehicle_request):
    """
        TERCEIRO MODO DE OPERAÇÃO

        Método que gerencia as solicitações dos veículos
        e solicita os pseudônimos para a PCA.
        Com butterfly key expansion.

        Recebe mensagem com:
            - 'recovery_point': ponto para decifrar o texto recebido,
            - 'cyphertext': valores cifrados do certificado.

        Após a decifração de cyphertext, obtemos a estrutura:
            - 'vehicle_id': Chave pública vitálicia (ponto) do veículo
            - 'pseud_point': Ponto que deve ser expandido em pseudônimos,
            - 'cypher_point': Ponto que deve ser expandido para cifrar o pacote de retorno,
            - 'key_p': Função de expansão para pseud_point (chave para AES),
            - 'key_c': Função de expansão para cypher_point (chave para AES)

        Solicita pre-linkage value para LA1 e LA2
        Envia pacote cifrado com
            - 'request_id': Código de identificação da requisição;
            - 'period_i': inteiro representando o período para a
            solicitação do pseudônimo (i);
            - 'j_certificates': inteiro representando quantos
            certificados são válidos no mesmo período de tempo (j);

        Realiza a butterfly key expansion em pseud_point e cypher_point, utilizando
        key_p e key_c, respectivamente.
        Calcula:
            Bi = A + f_k(i) * G,
        no qual:
            - f_k(i) = AES(0^(128) XOR i, key_*) || AES(1^(128) XOR i, key_*)
            - 0^(128) corresponde a um array de 0s com comprimento 128
            - AES(m, k) corresponde à mensagem m cifrada com o algoritmo AES
            utilizando a chave k
        Para que f_k(i) seja transformado em BigNumber, é obtido o valor de Hash
        e desse lê-se o binário (read_bin).

        Cria pacote para a PCA com:
            - 'request_id': Código de identificação do pedido
            - 'pseud_point': Ponto que deve ser assinado (obtido com a expansão,
            - 'cypher_point': Ponto que deve cifrar o pacote de retorno (obtido com a expansão)
            - 'la_package_1': Valor cifrado recebido da LA_1
            - 'recovery_point_la_1': Ponto para decifrar la_package_1
            - 'la_package_2': Valor cifrado recebido da LA_2
            - 'recovery_point_la_2': Ponto para decifrar la_package_2
        Monta uma lista com os valores de retorno da PCA e cifra tudo
        com cypher_point.
    """
    private, public = get_own_keys()

    bin_value = vehicle_request.get('recovery_point')
    recovery_point = Point()
    recovery_point.read_bin(bin_value)

    cyphertext = vehicle_request.get('cyphertext')

    decrypted_message = CryptoProtocols.ecies_decrypt(recovery_point, cyphertext, private)

    vehicle_id = decrypted_message.get('vehicle_id')

    pseud_point = decrypted_message.get('pseud_point')
    pseud = Point()
    pseud.read_bin(pseud_point)

    cypher_point = decrypted_message.get('cypher_point')
    cypher = Point()
    cypher.read_bin(cypher_point)

    key_p = decrypted_message.get('key_p')
    key_c = decrypted_message.get('key_c')

    bd_vehicle_request = bd_conversion.save_vehicle_butterfly_request(vehicle_id, pseud_point, cypher_point, key_p, key_c)

    # =======================
    # SOLICITAÇÃO PARA AS LAs
    # =======================

    public_LA1 = bd_conversion.get_LA_1_public_key()
    public_LA2 = bd_conversion.get_LA_2_public_key()

    hash_text = vehicle_id + pseud_point
    request_id = Hash.hash_sha256(hash_text)
    period_i = 1
    plain_text_LA = {
        'request_id': request_id,
        'period_i': period_i,
        'j_certificates': N_CERTIFICATES_SIMULTANEOUSLY
    }

    cyphertext_to_LA_1, recovery_point_to_LA_1 = CryptoProtocols.ecies_encrypt(public_LA1, plain_text_LA)
    message_to_send_LA_1 = {
        'cyphertext': cyphertext_to_LA_1,
        'recovery_point': recovery_point_to_LA_1.write_bin()
    }

    cyphertext_to_LA_2, recovery_point_to_LA_2 = CryptoProtocols.ecies_encrypt(public_LA2, plain_text_LA)
    message_to_send_LA_2 = {
        'cyphertext': cyphertext_to_LA_2,
        'recovery_point': recovery_point_to_LA_2.write_bin()
    }

    received_message_LA_1 = websocket.send_message_LA(message_to_send_LA_1, 1)
    received_message_LA_2 = websocket.send_message_LA(message_to_send_LA_2, 2)

    # LA 1
    bin_value_LA_1 = received_message_LA_1.get('recovery_point')
    recovery_point_from_LA_1 = Point()
    recovery_point_from_LA_1.read_bin(bin_value_LA_1)

    cyphertext_from_LA_1 = received_message_LA_1.get('cyphertext')

    decrypted_message_LA_1 = CryptoProtocols.ecies_decrypt(recovery_point_from_LA_1, cyphertext_from_LA_1, private)
    pre_linkage_values_LA_1 = decrypted_message_LA_1.get('pre_linkage_values')

    la_1_transaction = bd_conversion.save_la_butterfly_transaction(bd_vehicle_request, 1, request_id, period_i, N_CERTIFICATES_SIMULTANEOUSLY, cyphertext_from_LA_1, bin_value_LA_1)

    # LA 2
    bin_value_LA_2 = received_message_LA_2.get('recovery_point')
    recovery_point_from_LA_2 = Point()
    recovery_point_from_LA_2.read_bin(bin_value_LA_2)

    cyphertext_from_LA_2 = received_message_LA_2.get('cyphertext')

    decrypted_message_LA_2 = CryptoProtocols.ecies_decrypt(recovery_point_from_LA_2, cyphertext_from_LA_2, private)
    pre_linkage_values_LA_2 = decrypted_message_LA_2.get('pre_linkage_values')

    la_2_transaction = bd_conversion.save_la_butterfly_transaction(bd_vehicle_request, 2, request_id, period_i, N_CERTIFICATES_SIMULTANEOUSLY, cyphertext_from_LA_2, bin_value_LA_2)

    ra_butterfly_transaction = bd_conversion.create_ra_butterlfy_transaction(la_1_transaction, la_2_transaction)
    # ======================
    # Fim Solicitação LA 1
    # ======================

    # ======================
    # Butterfly Key Expansion
    # ======================
    pca_messages = []
    chave_publ_PCA = bd_conversion.get_PCA_public_key()

    zeros = "".rjust(128, '0')
    ones = "".rjust(128, '1')
    for i in range(1, N_CERTIFICATES_SIMULTANEOUSLY + 1):
        # Cálculo de f_k(i)
        first_half_plain_text = bytes_xor(zeros, str(i).rjust(128, '0'))
        iv_p_1 = os.urandom(16)
        iv_c_1 = os.urandom(16)
        first_half_encrypted_p = CryptoProtocols.aes_cbc_encrypt(first_half_plain_text, key_p, iv_p_1)
        first_half_encrypted_c = CryptoProtocols.aes_cbc_encrypt(first_half_plain_text, key_c, iv_c_1)
        # print("IV_P_1: %s" % iv_p_1)

        second_half_plain_text = bytes_xor(ones, str(i).rjust(128, '0'))
        iv_p_2 = os.urandom(16)
        iv_c_2 = os.urandom(16)
        second_half_encrypted_p = CryptoProtocols.aes_cbc_encrypt(second_half_plain_text, key_p, iv_p_2)
        second_half_encrypted_c = CryptoProtocols.aes_cbc_encrypt(second_half_plain_text, key_c, iv_c_2)
        # print("IV_P_2: %s" % iv_p_2)

        fk_p = first_half_encrypted_p + second_half_encrypted_p
        fk_c = first_half_encrypted_c + second_half_encrypted_c

        # fk_p as BigNumber
        big_fk_p = BigNumber()
        hashed_fk_p = bytearray(Hash.get_hash(fk_p), "utf-8")
        big_fk_p.read_bin(hashed_fk_p[0:BN_SIZE_ARRAY])
        # print("FK_P Big: %s" % big_fk_p)

        # fk_c as BigNumber
        big_fk_c = BigNumber()
        hashed_fk_c = bytearray(Hash.get_hash(fk_c), "utf-8")
        big_fk_c.read_bin(hashed_fk_c[0:BN_SIZE_ARRAY])
        # print("FK_c Big: %s" % big_fk_c)

        # Cálculo de Bi e Ji
        result_fkp_g = Point()
        result_B_i = Point()
        Point.multiply_by_generator(result_fkp_g, big_fk_p)
        Point.add(result_B_i, pseud, result_fkp_g)
        result_B_i_bin = result_B_i.write_bin()

        result_fkc_g = Point()
        result_J_i = Point()
        Point.multiply_by_generator(result_fkc_g, big_fk_c)
        Point.add(result_J_i, cypher, result_fkc_g)
        result_J_i_bin = result_J_i.write_bin()

        hash_pca_text = vehicle_id + result_B_i_bin
        request_pca_id = Hash.hash_sha256(hash_pca_text)

        la_cyphertext_1 = pre_linkage_values_LA_1[i - 1].get('cyphertext')
        la_cyphertext_2 = pre_linkage_values_LA_2[i - 1].get('cyphertext')

        la_recovery_point_1 = pre_linkage_values_LA_1[i - 1].get('recovery_point')
        la_recovery_point_2 = pre_linkage_values_LA_2[i - 1].get('recovery_point')

        pca_package = {
            'request_id': request_pca_id,
            'pseud_point': result_B_i_bin,
            'cypher_point': result_J_i_bin,
            'la_package_1': la_cyphertext_1,
            'recovery_point_la_1': la_recovery_point_1,
            'la_package_2': la_cyphertext_2,
            'recovery_point_la_2': la_recovery_point_2
        }
        # print("\n\npca_package: %s\n" % pca_package)
        pca_encrypted_message, pca_recovery_point = CryptoProtocols.ecies_encrypt(chave_publ_PCA, pca_package)

        key_expansion = bd_conversion.save_key_expansion(bd_vehicle_request, request_pca_id, i, iv_p_1, iv_p_2, iv_c_1, iv_c_2, la_cyphertext_1, la_recovery_point_1, la_cyphertext_2, la_recovery_point_2)
        ra_butterfly_transaction.key_expansions.add(key_expansion)

        message_to_send = {
            'recovery_point': pca_recovery_point.write_bin(),
            'cyphertext': pca_encrypted_message
        }
        pca_messages.append(message_to_send)

    # ==============================
    # Fim Butterfly Key Expansion
    # ==============================

    # TODO? SHUFFLE DE MENSAGENS DE DIFERENTES VEÍCULOS ANTES DE ENVIAR
    all_pseudonyms = []
    for message in pca_messages:
        received_message_PCA = websocket.send_message(message)

        bin_value_PCA = received_message_PCA.get('recovery_point')
        recovery_point_PCA = Point()
        recovery_point_PCA.read_bin(bin_value_PCA)

        cyphertext_PCA = received_message_PCA.get('cyphertext')

        decrypted_message_PCA = CryptoProtocols.ecies_decrypt(recovery_point_PCA, cyphertext_PCA, private)

        recovery_point_to_vehicle = decrypted_message_PCA.get('recovery_point')
        cyphertext_to_vehicle = decrypted_message_PCA.get('cyphertext')

        request_id_PCA = decrypted_message_PCA.get('request_id')
        updated_pca_request = bd_conversion.update_pca_request(request_id_PCA, cyphertext_to_vehicle, recovery_point_to_vehicle)
        if(updated_pca_request):
            key_expansion = bd_conversion.get_key_expansion_by_pca_request(updated_pca_request.pca_request_id)
            message_to_vehicle = {
                'recovery_point': recovery_point_to_vehicle,
                'cyphertext': cyphertext_to_vehicle,
                'iv_p_1': key_expansion.iv_p_1,
                'iv_p_2': key_expansion.iv_p_2,
                'iv_c_1': key_expansion.iv_c_1,
                'iv_c_2': key_expansion.iv_c_2,
                'expansion_index': key_expansion.expansion_number
            }
            all_pseudonyms.append(message_to_vehicle)
    plain_message_final = {
        'pseudonyms': all_pseudonyms
    }
    cyphertext_final, recovery_point_final = CryptoProtocols.ecies_encrypt(cypher, plain_message_final)
    final_message = {
        'recovery_point': recovery_point_final.write_bin(),
        'cyphertext': cyphertext_final
    }
    return final_message


def get_own_keys():
    """
    Retorna a chave pública (Point) e privada (BigNumber) da RA
    """
    private_bin, public_bin = bd_conversion.get_RA_keypair_values()
    private = BigNumber()
    public = Point()

    if private_bin and public_bin:
        # print("TEM CHAVE!")
        private.read_bin(private_bin)
        public.read_bin(public_bin)
    else:
        # ERRO
        # print("Criando chaves!")
        CryptoProtocols.generate_keys(private, public)
        bd_conversion.save_own_keys(private, public)

    return private, public


def bytes_xor(a, b):
    # return bytes(bytearray(x, "utf-8") ^ bytearray(y, "utf-8") for x, y in zip(a, b))
    return [ord(a) ^ ord(b) for a, b in zip(a, b)]
