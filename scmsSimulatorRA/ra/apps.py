from django.apps import AppConfig


class RaConfig(AppConfig):
    name = 'ra'

    def ready(self):
        from pyrelic import relic_config
        relic_config.setup_relic()
