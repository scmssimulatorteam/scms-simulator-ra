from django.db import models
from pyrelic.relic_constants import BIN_BN_SIZE
from pyrelic.relic_constants import BIN_EC_SIZE
from pyrelic.relic_constants import AES_KEY_SIZE
from django.core.validators import int_list_validator


class PrivateKey(models.Model):
    bin_value = models.CharField(max_length=BIN_BN_SIZE, validators=[int_list_validator])


class PublicKey(models.Model):
    bin_value = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class OwnerPublicKey(models.Model):
    """
        RA só terá:
        - sua própria chave pública (RA)
        - chave pública da PCA
    """
    PROFILES = (
        ('P', 'PCA'),
        ('R', 'RA'),
        ('L', 'LA1'),
        ('M', 'LA2'),
    )

    belong_to = models.CharField(max_length=1, choices=PROFILES)
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)


class KeyPair(models.Model):
    public_key = models.ForeignKey(PublicKey, on_delete=models.CASCADE)
    private_key = models.ForeignKey(PrivateKey, on_delete=models.CASCADE)


class VehicleRequest(models.Model):
    vehicle_id = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    pseud_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    cypher_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class VehicleReturnValue(models.Model):
    request_id = models.CharField(max_length=BIN_EC_SIZE)
    reconstruction_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    cyphertext = models.CharField(max_length=4000, validators=[int_list_validator])


class VehicleTransaction(models.Model):
    transaction_date = models.DateTimeField(auto_now_add=True)
    request = models.ForeignKey(VehicleRequest, on_delete=models.CASCADE)
    return_value = models.ForeignKey(VehicleReturnValue, on_delete=models.CASCADE)

# ===================
# 3o modo de operação
# ===================


class VehicleButterflyRequest(models.Model):
    vehicle_id = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    pseud_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    cypher_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    key_p = models.CharField(max_length=AES_KEY_SIZE, validators=[int_list_validator])
    key_c = models.CharField(max_length=AES_KEY_SIZE, validators=[int_list_validator])


class PcaButterflyTransaction(models.Model):
    transaction_date = models.DateTimeField(auto_now_add=True)
    pca_request_id = models.CharField(max_length=256)
    cyphertext = models.TextField(validators=[int_list_validator])
    recovery_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class KeyExpansion(models.Model):
    vehicle_request = models.ForeignKey(VehicleButterflyRequest, on_delete=models.CASCADE)
    pca_transaction = models.ForeignKey(PcaButterflyTransaction, on_delete=models.CASCADE)
    expansion_number = models.PositiveIntegerField()
    iv_p_1 = models.CharField(max_length=AES_KEY_SIZE, validators=[int_list_validator])
    iv_p_2 = models.CharField(max_length=AES_KEY_SIZE, validators=[int_list_validator])
    iv_c_1 = models.CharField(max_length=AES_KEY_SIZE, validators=[int_list_validator])
    iv_c_2 = models.CharField(max_length=AES_KEY_SIZE, validators=[int_list_validator])
    la_cyphertext_1 = models.CharField(max_length=4000, validators=[int_list_validator])
    la_recovery_point_1 = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])
    la_cyphertext_2 = models.CharField(max_length=4000, validators=[int_list_validator])
    la_recovery_point_2 = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class LaButterflyTransaction(models.Model):
    vehicle_request = models.ForeignKey(VehicleButterflyRequest, on_delete=models.CASCADE)
    la_number = models.PositiveIntegerField()
    request_id = models.CharField(max_length=256)  # hash size
    period_i = models.PositiveIntegerField()
    j_certificates = models.PositiveIntegerField()
    cyphertext = models.TextField(validators=[int_list_validator])
    reconstruction_point = models.CharField(max_length=BIN_EC_SIZE, validators=[int_list_validator])


class RaButterflyTransaction(models.Model):
    key_expansions = models.ManyToManyField(KeyExpansion)
    la_1_transaction = models.ForeignKey(LaButterflyTransaction, related_name='%(app_label)s_%(class)s_1', on_delete=models.CASCADE)
    la_2_transaction = models.ForeignKey(LaButterflyTransaction, related_name='%(app_label)s_%(class)s_2', on_delete=models.CASCADE)
