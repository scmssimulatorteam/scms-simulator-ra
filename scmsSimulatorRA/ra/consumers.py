# chat/consumers.py
from channels.generic.websocket import WebsocketConsumer
from ra import scms_cryptography
from scmsSimulatorRA.settings.base import CURRENT_SETTING, SECOND_MODE, THIRD_MODE
import json


class RaConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, close_code):
        pass

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        # print("Mensagem recebida: %s" % text_data_json)

        if('cyphertext' in text_data_json and 'recovery_point' in text_data_json):
            if(CURRENT_SETTING == SECOND_MODE):
                return_message = scms_cryptography.handle_vehicle_solicitation_second(text_data_json)
            elif(CURRENT_SETTING == THIRD_MODE):
                return_message = scms_cryptography.handle_vehicle_solicitation_third(text_data_json)
            else:
                return_message = "Algo não está certo!"
        else:
            return_message = "Retornei..."
        # print("Enviando: %s" % return_message)
        self.send(text_data=json.dumps({
            'message': return_message
        }))
