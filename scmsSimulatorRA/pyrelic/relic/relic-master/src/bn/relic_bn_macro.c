/*
 * RELIC is an Efficient LIbrary for Cryptography
 * Copyright (C) 2007-2015 RELIC Authors
 *
 * This file is part of RELIC. RELIC is legal property of its developers,
 * whose names are not listed here. Please refer to the COPYRIGHT file
 * for contact information.
 *
 * RELIC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * RELIC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with RELIC. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file
 *
 * Implementação das macros de BN
 *
 * @ingroup bn
 */

#include <errno.h>
#include "relic_core.h"
#include "relic_bn.h"

/*============================================================================*/
/* Public definitions                                                         */
/*============================================================================*/

/*============================================================================*/
/* relic_bn.h 		                                                          */
/*============================================================================*/

void bn_new_macro(bn_t A) {
	bn_init(A, BN_SIZE);
}

void bn_mul_macro(bn_t C, const bn_t A, const bn_t B) {
	bn_mul_comba(C, A, B);
}