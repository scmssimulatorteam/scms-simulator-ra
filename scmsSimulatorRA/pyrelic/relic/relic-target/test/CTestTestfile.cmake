# CMake generated Testfile for 
# Source directory: /home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-master/test
# Build directory: /home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_bn "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_bn")
add_test(test_dv "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_dv")
add_test(test_fp "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_fp")
add_test(test_fpx "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_fpx")
add_test(test_fb "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_fb")
add_test(test_fbx "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_fbx")
add_test(test_ep "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_ep")
add_test(test_epx "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_epx")
add_test(test_eb "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_eb")
add_test(test_ed "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_ed")
add_test(test_ec "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_ec")
add_test(test_pc "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_pc")
add_test(test_pp "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_pp")
add_test(test_md "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_md")
add_test(test_cp "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_cp")
add_test(test_rand "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_rand")
add_test(test_core "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/test_core")
