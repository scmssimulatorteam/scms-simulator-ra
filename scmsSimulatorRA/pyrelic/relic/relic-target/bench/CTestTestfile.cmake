# CMake generated Testfile for 
# Source directory: /home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-master/bench
# Build directory: /home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bench
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(bench_bn "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_bn")
add_test(bench_dv "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_dv")
add_test(bench_fp "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_fp")
add_test(bench_fpx "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_fpx")
add_test(bench_fb "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_fb")
add_test(bench_fbx "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_fbx")
add_test(bench_ep "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_ep")
add_test(bench_epx "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_epx")
add_test(bench_eb "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_eb")
add_test(bench_ed "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_ed")
add_test(bench_ec "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_ec")
add_test(bench_pp "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_pp")
add_test(bench_pc "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_pc")
add_test(bench_cp "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_cp")
add_test(bench_rand "/home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target/bin/bench_rand")
