# CMake generated Testfile for 
# Source directory: /home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-master
# Build directory: /home/osboxes/Git/scms-simulator/scms-simulator-ra/scmsSimulatorRA/pyrelic/relic/relic-target
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("test")
subdirs("bench")
