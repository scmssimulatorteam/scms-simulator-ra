# Simulador SCMS RA

### Pré-requisitos
- Python 3.7.0
- pip 10.0.1
- virtualenv 16.0.0
- PosgreSQL 10.4


## Configuração do banco de dados
- Acessar banco de dados:
```
sudo su postgres
psql
```

- Criar uma database para cada módulo:
```
CREATE DATABASE scmssimulatorra_second;
CREATE DATABASE scmssimulatorra_third 
```

- Criar o usuário e alterar suas configurações
```                                                                                          
create user scmsuser with password 'scmsprojectpassword';
ALTER ROLE scmsuser SET client_encoding TO 'utf8';
ALTER ROLE scmsuser SET default_transaction_isolation TO 'read committed';
GRANT ALL PRIVILEGES ON DATABASE scmssimulatorra_second TO scmsuser;
GRANT ALL PRIVILEGES ON DATABASE scmssimulatorra_third TO scmsuser;
ALTER ROLE scmsuser SET timezone TO 'UTC';
```

## Instalação

- Criar um ambiente virtual
```
virtualenv env --python=python3.6 
```

- Acessar o ambiente virtual
```
source env/bin/activate
```

- Compilar a biblioteca e instalar requisitos através do Makefile
```
make all
```

- Acessar a pasta do projeto que deseja rodar. Exemplo: scmsSimulatorVehicle.
```
cd scmsSimulatorRA
```

- Aplicar migrações nos bancos de dados:
```
python manage.py migrate --settings=scmsSimulatorRA.settings.second
python manage.py migrate --settings=scmsSimulatorRA.settings.third
```

- Executar scripts no banco de dados:
```
\connect scmssimulatorra_second
-- Adiciona própria chave
INSERT INTO public.ra_publickey(bin_value)
    VALUES ('[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]');

INSERT INTO public.ra_privatekey(bin_value)
	VALUES ('[15,231,95,62,219,31,235,44,184,5,171,237,109,14,124,245,141,148,119,48,16,255,50,58,149,92,39,159,113,252,40,52]');

INSERT INTO public.ra_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('R', (SELECT id from public.ra_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1));

INSERT INTO public.ra_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.ra_privatekey WHERE bin_value='[15,231,95,62,219,31,235,44,184,5,171,237,109,14,124,245,141,148,119,48,16,255,50,58,149,92,39,159,113,252,40,52]' LIMIT 1),
		(SELECT id from public.ra_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1)
		);

-- Adiciona chave da PCA
INSERT INTO public.ra_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.ra_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.ra_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]' LIMIT 1));


\connect scmssimulatorra_third

-- Adiciona própria chave
INSERT INTO public.ra_publickey(bin_value)
    VALUES ('[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]');

INSERT INTO public.ra_privatekey(bin_value)
	VALUES ('[15,231,95,62,219,31,235,44,184,5,171,237,109,14,124,245,141,148,119,48,16,255,50,58,149,92,39,159,113,252,40,52]');

INSERT INTO public.ra_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('R', (SELECT id from public.ra_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1));

INSERT INTO public.ra_keypair(private_key_id, public_key_id)
	VALUES (
		(SELECT id from public.ra_privatekey WHERE bin_value='[15,231,95,62,219,31,235,44,184,5,171,237,109,14,124,245,141,148,119,48,16,255,50,58,149,92,39,159,113,252,40,52]' LIMIT 1),
		(SELECT id from public.ra_publickey WHERE bin_value='[3,0,55,134,248,105,18,50,10,6,179,130,226,179,35,235,138,23,2,231,228,203,56,63,164,54,174,127,108,68,139,31,123]' LIMIT 1)
		);


-- Adiciona chave da PCA
INSERT INTO public.ra_publickey(bin_value)
    VALUES ('[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]');

INSERT INTO public.ra_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('P', (SELECT id from public.ra_publickey WHERE bin_value='[2,26,103,209,130,133,76,132,35,153,82,179,29,109,186,250,201,1,111,53,214,77,183,209,171,154,152,91,143,183,242,109,166]' LIMIT 1));


-- Adiciona chave da LA1
INSERT INTO public.ra_publickey(bin_value)
    VALUES ('[3,94,22,139,172,56,30,205,186,159,45,156,6,37,239,186,71,26,128,37,197,177,20,60,155,120,53,191,75,241,157,47,175]');

INSERT INTO public.ra_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('L', (SELECT id from public.ra_publickey WHERE bin_value='[3,94,22,139,172,56,30,205,186,159,45,156,6,37,239,186,71,26,128,37,197,177,20,60,155,120,53,191,75,241,157,47,175]' LIMIT 1));

-- Adiciona chave da LA2
INSERT INTO public.ra_publickey(bin_value)
    VALUES ('[3,54,8,69,56,231,210,116,125,115,251,29,38,2,186,17,72,12,194,176,182,107,180,161,13,149,89,252,235,133,150,99,228]');

INSERT INTO public.ra_ownerpublickey(
            belong_to, public_key_id)
    VALUES ('M', (SELECT id from public.ra_publickey WHERE bin_value='[3,54,8,69,56,231,210,116,125,115,251,29,38,2,186,17,72,12,194,176,182,107,180,161,13,149,89,252,235,133,150,99,228]' LIMIT 1));
```

- Executar o programa com o modo de operação desejado
```
python manage.py runserver 9000 --settings=scmsSimulatorRA.settings.second
```


## Observações

- Se for necessário compilar apenas a biblioteca novamente, basta executar o comando
```
make relic
```

- Login admin
```
user: giuliana
senha: scmsadmin
```