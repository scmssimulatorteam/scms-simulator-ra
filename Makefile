.PHONY: all init relic clean test dist
.DEFAULT_GOAL := all

init:
	pip install -r requirements.txt
	sudo apt-get install libgmp3-dev

dist:
	python setup.py sdist

relic:
	if [ ! -d scmsSimulatorRA/pyrelic/relic/relic-target ]; then mkdir scmsSimulatorRA/pyrelic/relic/relic-target; fi; cd scmsSimulatorRA/pyrelic/relic/relic-target; ../relic-master/preset/x64-pbc-128.sh ../relic-master/; make clean; make;

clean:
	if [ -d scmsSimulatorRA/pyrelic/relic/relic-target ]; then rm -r scmsSimulatorRA/pyrelic/relic/relic-target; fi; if [ -d build/ ]; then rm -r build/; fi;  if [ -d dist/ ]; then rm -r dist/; fi; if [ -d *.egg-info ]; then rm -r *.egg-info; fi; 

test:
	cd scmsSimulatorRA; python -m unittest discover -v; cd ..; 

all: init clean relic
